# HPV type-specific attribution in cervical cancer

This repository contains the scripts used to produce the main results for **Wei et al. (2023)** publication.


The main model used in the analysis is `script/hpv_or_and_prev_fixed_01a.stan` which produces the **posterior distribution** of **type-specific** global **OR** and country/region - specific **prevalence of HPV in cervical cancer** cases.

Modelling outputs and **AF scaling** is done afterward within `script/hpv_or_prev_output_01.sb`


Overall modelling procedure have been launched on IARC HPC (Osiris) via the sbatch script `script/hpv_or_prev_01.sb`.

Data formatting and model choice is performed in `script/hpv_or_prev_01.R`.


