functions {
}
data {
  // Data for OR calculation
  int<lower=1> N_or;  // total number of observations
  int Y_or[N_or];  // response variable
  int trials_or[N_or];  // number of trials
  int<lower=1> K_or;  // number of population-level effects
  matrix[N_or, K_or] X_or;  // population-level design matrix
  int<lower=1> Kc_or;  // number of population-level effects after centering
  
  // Data for  regional prev in cases calculation
  int<lower=1> N_pcr;  // total number of observations
  int Y_pcr[N_pcr];  // response variable
  int trials_pcr[N_pcr];  // number of trials
  int<lower=1> K_pcr;  // number of population-level effects
  matrix[N_pcr, K_pcr] X_pcr;  // population-level design matrix

  // Data for global prev in cases calculation
  int<lower=1> N_pcg;  // total number of observations
  int Y_pcg[N_pcg];  // response variable
  int trials_pcg[N_pcg];  // number of trials
  int<lower=1> K_pcg;  // number of population-level effects
  matrix[N_pcg, K_pcg] X_pcg;  // population-level design matrix

  int prior_only;  // should the likelihood be ignored?
}
transformed data {
  matrix[N_or, Kc_or] Xc_or;  // centered version of X without an intercept
  vector[Kc_or] means_X_or;  // column means of X before centering
  for (i in 2:K_or) {
    means_X_or[i - 1] = mean(X_or[, i]);
    Xc_or[, i - 1] = X_or[, i] - means_X_or[i - 1];
  }
}
parameters {
  // params for OR model
  vector[Kc_or] b_or;  // regression coefficients
  real Intercept_or;  // temporary intercept for centered predictors

  // params for regional prev in cases model
  vector[K_pcr] b_pcr;  // regression coefficients

  // params for global prev in cases model
  vector[K_pcg] b_pcg;  // regression coefficients
}
transformed parameters {
  // transformed params for OR model
  real lprior_or = 0;  // prior contributions to the log posterior

  // transformed params for regional prev in cases model
  real lprior_pcr = 0;  // prior contributions to the log posterior

  // transformed params for global prev in cases model
  real lprior_pcg = 0;  // prior contributions to the log posterior

}
model {
  // OR model
  // likelihood including constants
  if (!prior_only) {
    // initialize linear predictor term
    vector[N_or] mu_or = rep_vector(0.0, N_or);
    mu_or += Intercept_or + Xc_or * b_or;
    target += binomial_logit_lpmf(Y_or | trials_or, mu_or);
  }
  // priors including constants
  target += lprior_or;

  // regional prev in cases model
  // likelihood including constants
  if (!prior_only) {
    // initialize linear predictor term
    vector[N_pcr] mu_pcr = rep_vector(0.0, N_pcr);
    mu_pcr += X_pcr * b_pcr;
    target += binomial_logit_lpmf(Y_pcr | trials_pcr, mu_pcr);
  }
  // priors including constants
  target += lprior_pcr;

  // global prev in cases model
  // likelihood including constants
  if (!prior_only) {
    // initialize linear predictor term
    vector[N_pcg] mu_pcg = rep_vector(0.0, N_pcg);
    mu_pcg += X_pcg * b_pcg;
    target += binomial_logit_lpmf(Y_pcg | trials_pcg, mu_pcg);
  }
  // priors including constants
  target += lprior_pcg;
}
generated quantities {
  // actual population-level intercept
  real OR = exp(b_or[1]);
  vector[K_pcr] PICr = inv_logit(b_pcr);
  vector[K_pcg] PICg = inv_logit(b_pcg);
}

