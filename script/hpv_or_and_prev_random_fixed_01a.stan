functions {
}
data {
  // Data for OR calculation
  int<lower=1> N_or;  // total number of observations
  int Y_or[N_or];  // response variable
  int trials_or[N_or];  // number of trials
  int<lower=1> K_or;  // number of population-level effects
  matrix[N_or, K_or] X_or;  // population-level design matrix
  int<lower=1> Kc_or;  // number of population-level effects after centering
  // data for group-level effects of ID 1
  int<lower=1> N_1_or;  // number of grouping levels
  int<lower=1> M_1_or;  // number of coefficients per level
  int<lower=1> J_1_or[N_or];  // grouping indicator per observation
  // group-level predictor values
  vector[N_or] Z_1_1_or;
  
  // Data for  regional prev in cases calculation
  int<lower=1> N_pcr;  // total number of observations
  int Y_pcr[N_pcr];  // response variable
  int trials_pcr[N_pcr];  // number of trials
  int<lower=1> K_pcr;  // number of population-level effects
  matrix[N_pcr, K_pcr] X_pcr;  // population-level design matrix
  
  // Data for country specific prev in cases calculation
  int<lower=1> N_pcc;  // total number of observations
  int Y_pcc[N_pcc];  // response variable
  int trials_pcc[N_pcc];  // number of trials
  int<lower=1> K_pcc;  // number of population-level effects
  matrix[N_pcc, K_pcc] X_pcc;  // population-level design matrix
  
  // Data for global prev in cases calculation
  int<lower=1> N_pcg;  // total number of observations
  int Y_pcg[N_pcg];  // response variable
  int trials_pcg[N_pcg];  // number of trials
  int<lower=1> K_pcg;  // number of population-level effects
  matrix[N_pcg, K_pcg] X_pcg;  // population-level design matrix
  
  int prior_only;  // should the likelihood be ignored?
}
transformed data {
  matrix[N_or, Kc_or] Xc_or;  // centered version of X without an intercept
  vector[Kc_or] means_X_or;  // column means of X before centering
  for (i in 2:K_or) {
    means_X_or[i - 1] = mean(X_or[, i]);
    Xc_or[, i - 1] = X_or[, i] - means_X_or[i - 1];
  }
}
parameters {
  // params for OR model
  vector[Kc_or] b_or;  // regression coefficients
  real Intercept_or;  // temporary intercept for centered predictors
  vector<lower=0>[M_1_or] sd_1_or;  // group-level standard deviations
  vector[N_1_or] z_1_or[M_1_or];  // standardized group-level effects
  
  // params for regional prev in cases model
  vector[K_pcr] b_pcr;  // regression coefficients
 
  // params for country specific prev in cases model
  vector[K_pcc] b_pcc;  // regression coefficients

  // params for global prev in cases model
  vector[K_pcg] b_pcg;  // regression coefficients
}
transformed parameters {
  // transformed params for OR model
  vector[N_1_or] r_1_1_or;  // actual group-level effects
  real lprior_or = 0;  // prior contributions to the log posterior
  r_1_1_or = (sd_1_or[1] * (z_1_or[1]));
  lprior_or += student_t_lpdf(Intercept_or | 3, 0, 2.5);
  lprior_or += student_t_lpdf(sd_1_or | 3, 0, 2.5)
    - 1 * student_t_lccdf(0 | 3, 0, 2.5);
    
  // transformed params for regional prev in cases model
  real lprior_pcr = 0;  // prior contributions to the log posterior
   
  // transformed params for country specific prev in cases model
  real lprior_pcc = 0;  // prior contributions to the log posterior
   
  // transformed params for global prev in cases model
  real lprior_pcg = 0;  // prior contributions to the log posterior
}
model {
  // OR model
  // likelihood including constants
  if (!prior_only) {
    // initialize linear predictor term
    vector[N_or] mu_or = rep_vector(0.0, N_or);
    mu_or += Intercept_or + Xc_or * b_or;
    for (n_or in 1:N_or) {
      // add more terms to the linear predictor
      mu_or[n_or] += r_1_1_or[J_1_or[n_or]] * Z_1_1_or[n_or];
    }
    target += binomial_logit_lpmf(Y_or | trials_or, mu_or);
  }
  // priors including constants
  target += lprior_or;
  target += std_normal_lpdf(z_1_or[1]);
  
  // regional prev in cases model
  // likelihood including constants
  if (!prior_only) {
    // initialize linear predictor term
    vector[N_pcr] mu_pcr = rep_vector(0.0, N_pcr);
    mu_pcr += X_pcr * b_pcr;
    target += binomial_logit_lpmf(Y_pcr | trials_pcr, mu_pcr);
  }
  // priors including constants
  target += lprior_pcr;
  
  // country specific prev in cases model
  // likelihood including constants
  if (!prior_only) {
    // initialize linear predictor term
    vector[N_pcc] mu_pcc = rep_vector(0.0, N_pcc);
    mu_pcc += X_pcc * b_pcc;
    target += binomial_logit_lpmf(Y_pcc | trials_pcc, mu_pcc);
  }
  // priors including constants
  target += lprior_pcc;
  
  // global prev in cases model
  // likelihood including constants
  if (!prior_only) {
    // initialize linear predictor term
    vector[N_pcg] mu_pcg = rep_vector(0.0, N_pcg);
    mu_pcg += X_pcg * b_pcg;
    target += binomial_logit_lpmf(Y_pcg | trials_pcg, mu_pcg);
  }
  // priors including constants
  target += lprior_pcg;
}
generated quantities {
  // actual population-level intercept
  // real b_Intercept_or = Intercept_or - dot_product(means_X_or, b_or);
  real OR = exp(b_or[1]);
  vector[K_pcr] PICr = inv_logit(b_pcr);
  vector[K_pcc] PICc = inv_logit(b_pcc);
  vector[K_pcg] PICg = inv_logit(b_pcg);
}

